create database crypto;
use crypto;
SET SQL_SAFE_UPDATES = 0;
set global sql_mode="";


create table UnregisterUser (
	id INT unique not null auto_increment,
	firstName varchar(256) not null,
    lastName varchar(256) not null,
    email varchar(256) unique not null,
    registerToken varchar(256) unique not null,
    expiryDate timestamp not null,
    password varchar(256) not null,
    primary key(id)
    );

create table User (
	id varchar(256) unique not null,
	firstName varchar(256) not null,
    lastName varchar(256) not null,
    email varchar(256) unique not null,
    primary key(id)
    );

create table UserAuthority (
	email varchar(256) unique not null,
    password varchar(1024) not null,
    userId varchar(256) not null,
    lastChange timestamp not null,
    foreign key(userId) references User(id),
	primary key(email)
    );

create table SecurityKey (
	userId varchar(256) unique not null,
    securityKey varchar(1024) not null,
    foreign key(userId) references User(id),
	primary key(userId)
    );
