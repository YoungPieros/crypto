package dto;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

public class SteganographyImageDto {

    private BufferedImage bufferedImage;
    private InputStream inputStream;
    private String fileName;
    private String password;
    private String text;
    private String contentType;

    public SteganographyImageDto(String fileName, String password, String text, String contentType, InputStream inputStream) {
        this.fileName = fileName;
        this.password = password;
        this.text = text;
        this.contentType = contentType;
        this.inputStream = inputStream;
        this.readBufferImage();
    }

    public SteganographyImageDto(String fileName, String password, String text, String contentType, BufferedImage bufferedImage) {
        this.fileName = fileName;
        this.password = password;
        this.text = text;
        this.contentType = contentType;
        this.bufferedImage = bufferedImage;
    }

    private void readBufferImage () {
        try {
            bufferedImage = ImageIO.read(inputStream);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public BufferedImage getBufferedImage() {
        return bufferedImage;
    }

    public String getFileName() {
        return fileName;
    }

    public String getPassword() {
        return password;
    }

    public String getText() {
        return text;
    }

    public String getContentType() {
        return contentType;
    }

    public void setBufferedImage(BufferedImage bufferedImage) {
        this.bufferedImage = bufferedImage;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
}
