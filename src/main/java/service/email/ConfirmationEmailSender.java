package service.email;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
@Service
public class ConfirmationEmailSender implements EmailService {

    @Autowired
    private JavaMailSender javaMailSender;

    @Value("${app.domain.address}")
    private String domain;
    @Value("${spring.mail.properties.mail.smtp.from}")
    private String SENDER_EMAIL_ADDRESS;

    public void sendConfirmationEmail (String emailAddress, String confirmationToken) {
        String subject = "Complete Registration In Crypto";
        String confirmationLink = domain + "/user/confirm-email?token=" + confirmationToken;
        String text = "To confirm your account, please click on below link:\n" + confirmationLink;
        this.sendEmail(emailAddress, subject, text);
    }

    @Async
    @Override
    public void sendEmail(String receiverEmail, String subject, String text) {
        SimpleMailMessage email = new SimpleMailMessage();
        email.setFrom(SENDER_EMAIL_ADDRESS);
        email.setTo(receiverEmail);
        email.setSubject(subject);
        email.setText(text);
        javaMailSender.send(email);
    }

}
