package service.email;

public interface EmailService {

    public void sendEmail (String receiverEmail, String Subject, String text);

}
