package service.registeration;

import domain.account.AccountCreator;
import domain.entity.UnregisterUser;
import org.springframework.stereotype.Service;
import repository.proxy.ProxyUnregisterAccountRepository;
import security.registeration.exception.ExpireRegisterationToken;
import security.registeration.exception.InvalidConfirmEmailToken;

import java.util.Date;

@Service
public class EmailConfirmationVerifier {

    public void confirmEmail (String confirmToken) throws InvalidConfirmEmailToken, ExpireRegisterationToken {
        UnregisterUser unregisterUser = null;
        try {
            unregisterUser = ProxyUnregisterAccountRepository.getRepository().loadUnregisterUser(confirmToken);
            if (unregisterUser == null)
                throw new InvalidConfirmEmailToken();
            if (isRegisterationTokenExpired(unregisterUser))
                throw new ExpireRegisterationToken();
            AccountCreator accountBuilder = AccountCreator.getBuilder();
            accountBuilder.buildAccount(unregisterUser);
        }
        finally {
            if (unregisterUser != null)
                ProxyUnregisterAccountRepository.getRepository().deleteUnregisterUser(unregisterUser);
        }
    }

    private boolean isRegisterationTokenExpired (UnregisterUser unregisterUser) {
        return unregisterUser.getExpiryDate().before(new Date());
    }

}
