package web_services;


import domain.entity.UserAuthority;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import security.authentication.AuthenticationManager;
import security.authentication.jwt.JwtAuthenticationResponse;
import security.authentication.exception.WrongCredential;

@RestController
@RequestMapping("/user")
public class UserLogin {

    @Autowired
    private AuthenticationManager authenticationManager;


    @RequestMapping(value = "/login", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> loginAccount ()
            throws WrongCredential {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String jwtToken = null;
        if (authenticationManager.authenticate(userDetails.getUsername(), userDetails.getPassword()))
            jwtToken = authenticationManager.getJWT();
        return ResponseEntity.ok(new JwtAuthenticationResponse(jwtToken));
    }


}
