package web_services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import security.registeration.exception.InvalidConfirmEmailToken;
import service.registeration.EmailConfirmationVerifier;
import security.registeration.exception.ExpireRegisterationToken;

@RestController
@RequestMapping("/user")
public class EmailConfirmation {

    @Autowired
    private EmailConfirmationVerifier emailConfirmationVerifier;

    @RequestMapping(value = "/confirm-email", method = RequestMethod.GET)
    public void confirmAccount (@RequestParam("token") String confirmToken)
            throws InvalidConfirmEmailToken, ExpireRegisterationToken {
        emailConfirmationVerifier.confirmEmail(confirmToken);
    }


}
