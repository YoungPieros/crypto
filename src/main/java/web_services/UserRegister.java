package web_services;


import domain.account.UnregisterAccountCreator;
import dto.UserAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import security.registeration.validator.RepetitiveEmailValidator;
import security.registeration.exception.RegistrationArgument;
import security.registeration.validator.RegistrationArgumentValidator;
import security.registeration.validator.RegistrationFormArgumentValidator;
import service.email.ConfirmationEmailSender;


@RestController
@RequestMapping("/user")
public class UserRegister {

    @Autowired
    private ConfirmationEmailSender confirmationEmailService;

    @RequestMapping (value = "/register", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public UserAccount registerAccount (
            @RequestParam(value = "firstName") String firstName,
            @RequestParam(value = "lastName") String lastName,
            @RequestParam(value = "email") String email,
            @RequestParam(value = "password") String password)
                throws RegistrationArgument {
        UserAccount userAccount = new UserAccount(firstName, lastName, email, password);
        RegistrationArgumentValidator validator = new RegistrationFormArgumentValidator(
                                                        new RepetitiveEmailValidator());
        validator.validate(userAccount);
        UnregisterAccountCreator accountBuilder = new UnregisterAccountCreator();
        accountBuilder.buildAccount(userAccount);
        confirmationEmailService.sendConfirmationEmail(email, userAccount.getRegisterToken());
        return userAccount;
    }

}
