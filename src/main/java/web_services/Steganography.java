package web_services;

import domain.entity.SecurityKey;
import repository.proxy.ProxySecurityRepository;
import steganography.SteganographyImageEncoder;
import steganography.SteganographyStrategyEncryptor;
import steganography.exception.SteganographyException;
import domain.entity.UserAuthority;
import dto.SteganographyImageDto;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.annotation.MultipartConfig;
import java.io.IOException;

@RestController
@MultipartConfig
public class Steganography {

    @RequestMapping(value = "/steganography/encryption", method = RequestMethod.POST,
                    consumes = MediaType.MULTIPART_FORM_DATA_VALUE
    )
    public ResponseEntity<?> encodeTextInImage(@RequestParam("image") MultipartFile image,
                                               @RequestParam("text") String text,
                                               @RequestParam("password") String password)
            throws IOException, SteganographyException {
        String userId = ((UserAuthority) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUserId();
        SteganographyImageDto imageDto = new SteganographyImageDto(image.getOriginalFilename(), password, text, image.getContentType(), image.getInputStream());
        SecurityKey securityKey = ProxySecurityRepository.getRepository().load(userId);
        SteganographyImageEncoder encryptor = SteganographyStrategyEncryptor.createEncryptor(securityKey, imageDto);
        encryptor.encrypt();
        return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(imageDto.getContentType()))
                    .header(HttpHeaders. CONTENT_DISPOSITION, "attachment; filename=\"" + image.getOriginalFilename() + "\"")
                    .body(encryptor.getEncodedImageBytes());
    }

    @RequestMapping(value = "/steganography/decryption", method = RequestMethod.POST,
                    consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<?> extractHiddenTextFromImage (@RequestParam("image") MultipartFile image,
                                                         @RequestParam("password") String password)
            throws IOException, SteganographyException {
        String userId = ((UserAuthority) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUserId();
        SteganographyImageDto imageDto = new SteganographyImageDto(image.getOriginalFilename(), password, "", image.getContentType(), image.getInputStream());
        SecurityKey securityKey = ProxySecurityRepository.getRepository().load(userId);
        SteganographyImageEncoder decryptor = SteganographyStrategyEncryptor.createEncryptor(securityKey, imageDto);
        decryptor.decrypt();
        return ResponseEntity.ok().body(decryptor.getDecodedText());
    }

}
