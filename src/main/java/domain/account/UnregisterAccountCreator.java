package domain.account;

import domain.entity.UnregisterUser;
import dto.UserAccount;
import repository.proxy.ProxyUnregisterAccountRepository;
import security.password.PasswordEncoder;
import security.utils.ExpirationDateBuilder;
import security.registeration.EmailConfirmationToken;

public class UnregisterAccountCreator implements AccountBuilder {

    private UserAccount userAccount;
    private UnregisterUser unregisterUser;
    private final int EXPIRY_TIME_IN_MINUTE = 24 * 60;

    public void buildAccount(UserAccount userAccount) {
        this.userAccount = userAccount;
        this.buildAccount();
    }

    @Override
    public void buildAccount() {
        this.buildProfile();
        this.buildSecurityKey();
        this.buildPersonalInformation();
        this.createEmailConfirmation();
        ProxyUnregisterAccountRepository.getRepository().saveUnregisterUser(unregisterUser);
    }

    private void createEmailConfirmation () {
        EmailConfirmationToken emailConfirmationToken = new EmailConfirmationToken(unregisterUser);
        String confirmationToken = emailConfirmationToken.getConfirmationToken();
        unregisterUser.setRegisterToken(confirmationToken);
        userAccount.setRegisterToken(confirmationToken);
        unregisterUser.setExpiryDate(ExpirationDateBuilder.createExpirationDate(EXPIRY_TIME_IN_MINUTE));
    }

    @Override
    public void buildProfile() {
        unregisterUser = new UnregisterUser();
        unregisterUser.setFirstName(userAccount.getFirstName());
        unregisterUser.setLastName(userAccount.getLastName());
        unregisterUser.setEmail(userAccount.getEmail());
        String encodedPassword = (new PasswordEncoder()).encode(userAccount.getPassword());
        unregisterUser.setPassword(encodedPassword);
    }

    @Override
    public void buildSecurityAuthority() {

    }

    @Override
    public void buildSecurityKey() {

    }

    @Override
    public void buildPersonalInformation() {

    }
}
