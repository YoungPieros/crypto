package domain.account;


import domain.entity.SecurityKey;
import domain.entity.UnregisterUser;
import domain.entity.User;
import domain.entity.UserAuthority;
import domain.entity.mapper.UnregisterUserMapper;
import repository.proxy.ProxyAccountRepository;
import repository.proxy.ProxySecurityRepository;
import repository.proxy.ProxyUserAuthorityRepository;
import security.key_factory.SecurityKeyCreator;
import java.util.Date;


public class AccountCreator implements AccountBuilder {

    private UnregisterUser unregisterUser;

    private User user;
    private UserAuthority userAuthority;
    private SecurityKey securityKey;

    public void buildAccount(UnregisterUser unregisterUser) {
        this.unregisterUser = unregisterUser;
        buildAccount();
        saveAccount();
    }

    public static AccountCreator getBuilder () {
        return new AccountCreator();
    }

    @Override
    public void buildAccount () {
        this.buildProfile();
        this.buildSecurityKey();
        this.buildSecurityAuthority();
        this.buildPersonalInformation();

    }

    @Override
    public void buildProfile() {
        user = UnregisterUserMapper.toUser(unregisterUser);
    }

    @Override
    public void buildSecurityAuthority() {
        userAuthority = new UserAuthority();
        userAuthority.setEmail(unregisterUser.getEmail());
        userAuthority.setPassword(unregisterUser.getPassword());
        userAuthority.setLastChange(new Date());
        user.setUserAuthority(userAuthority);
        userAuthority.setUser(user);
    }

    @Override
    public void buildSecurityKey() {
        securityKey = SecurityKeyCreator.getInstance().createKey();
        user.setSecurityKey(securityKey);
        securityKey.setUser(user);
    }

    @Override
    public void buildPersonalInformation() {
    }

    private void saveAccount () {
        ProxyAccountRepository.getRepository().saveUser(user);
        ProxyUserAuthorityRepository.getRepository().save(userAuthority);
        ProxySecurityRepository.getRepository().save(securityKey);
    }
}
