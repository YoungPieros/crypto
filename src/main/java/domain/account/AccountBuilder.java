package domain.account;


public interface AccountBuilder {

    public void buildAccount ();

    public void buildProfile ();

    public void buildSecurityKey ();

    public void buildSecurityAuthority ();
    
    public void buildPersonalInformation ();

}
