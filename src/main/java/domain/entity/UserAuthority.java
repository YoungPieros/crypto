package domain.entity;

import java.util.Date;

public class UserAuthority {

    private String userId;
    private String email;
    private String password;
    private User user;
    private Date lastChange;

    public String getUserId() {
        return userId;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public User getUser() {
        return user;
    }

    public Date getLastChange() {
        return lastChange;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setLastChange(Date lastChange) {
        this.lastChange = lastChange;
    }

    @Override
    public String toString() {
        return "UserAuthority{" +
                "userId='" + userId + '\'' +
                ", email='" + email + '\'' +
                ", lastChange=" + lastChange +
                '}';
    }
}
