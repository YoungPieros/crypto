package domain.entity;

public class SecurityKey {

    private String userId;
    private String securityKey;
    private User user;

    public String getUserId() {
        return userId;
    }

    public String getSecurityKey() {
        return securityKey;
    }

    public User getUser() {
        return user;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setSecurityKey(String securityKey) {
        this.securityKey = securityKey;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
