package domain.entity;


import java.util.Date;


public class UnregisterUser {

    private int id;
    private String firstName;
    private String lastName;
    private String email;
    private String registerToken;
    private Date expiryDate;
    private String password;

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getRegisterToken() {
        return registerToken;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public String getPassword() {
        return password;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setRegisterToken(String registerToken) {
        this.registerToken = registerToken;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "UnregisterUser{" +
                "id='" + id + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", registerToken='" + registerToken + '\'' +
                '}';
    }
}
