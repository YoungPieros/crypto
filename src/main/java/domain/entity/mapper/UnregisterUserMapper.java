package domain.entity.mapper;

import domain.entity.UnregisterUser;
import domain.entity.User;

public class UnregisterUserMapper {

    public static User toUser (UnregisterUser unregisterUser) {
        User user = new User();
        user.setFirstName(unregisterUser.getFirstName());
        user.setLastName(unregisterUser.getLastName());
        user.setEmail(unregisterUser.getEmail());
        return user;
    }

}
