package domain.entity;


public class User {

    private String id;
    private String email;
    private String firstName;
    private String lastName;
    private SecurityKey securityKey;
    private UserAuthority userAuthority;

    public String getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public SecurityKey getSecurityKey() {
        return securityKey;
    }

    public UserAuthority getUserAuthority() {
        return userAuthority;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setSecurityKey(SecurityKey securityKey) {
        this.securityKey = securityKey;
    }

    public void setUserAuthority(UserAuthority userAuthority) {
        if (userAuthority != null)
            userAuthority.setUser(this);
        this.userAuthority = userAuthority;
    }

    @Override
    public String toString() {
        return "User{" +
                ", email='" + email + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
