package steganography.encryption.cryption;

import steganography.exception.SteganographyException;

public interface KeyBaseEncoder {

    public String encrypt(String text, String secret) throws SteganographyException;

    public String decrypt(String encodedText, String secret) throws SteganographyException;

}
