package steganography.encryption.cryption;

import steganography.encryption.EndMarker;
import steganography.encryption.Utils;
import steganography.exception.SteganographyException;

public abstract class KeyBaseDecoratorEncoder implements KeyBaseEncoder, EndMarker {

    private final KeyBaseDecoratorEncoder decoratorEncoder;
    private String endMark = "$$$$$$$";

    public KeyBaseDecoratorEncoder (KeyBaseDecoratorEncoder decoratorEncoder) {
        this.decoratorEncoder = decoratorEncoder;
    }

    public KeyBaseDecoratorEncoder (KeyBaseDecoratorEncoder decoratorEncoder, String endMark) {
        this.decoratorEncoder = decoratorEncoder;
        this.endMark = endMark;
    }

    @Override
    public String removeEndMark(String encodedText) {
        return Utils.removeEndMark(encodedText, endMark);
    }

    @Override
    public String appendEndMark(String encodedText) {
        return Utils.addEndMark(encodedText, endMark);
    }

    @Override
    public boolean hasEndMarker (String encodedText) {
        return Utils.isEndMark(encodedText, endMark);
    }

    public KeyBaseDecoratorEncoder getDecoratorEncoder() {
        return decoratorEncoder;
    }

    public String encryptWithDecorator(String encodedText, String secret) throws SteganographyException {
        if (decoratorEncoder == null)
            return encodedText;
        return decoratorEncoder.encrypt(encodedText, secret);
    }

    public String decryptWithDecorator(String decodedText, String secret) throws SteganographyException {
        if (decoratorEncoder == null)
            return decodedText;
        return decoratorEncoder.decrypt(decodedText, secret);
    }

    public abstract String encrypt(String text, String secret) throws SteganographyException;

    public abstract String decrypt(String encodedText, String secret) throws SteganographyException;

}
