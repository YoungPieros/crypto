package steganography.encryption.cryption.cryptor;

import steganography.encryption.cryption.KeyBaseDecoratorEncoder;
import steganography.exception.CipherException;
import steganography.exception.SteganographyException;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;


public class CipherEncoder extends KeyBaseDecoratorEncoder {


    private static final String ALGORITHM = "AES";
    private static final int KEY_LENGTH = 16;

    private static final String END_MARK = "#######";

    public CipherEncoder() {
        super(null, END_MARK);
    }

    public CipherEncoder(KeyBaseDecoratorEncoder decoratorEncoder) {
        super(decoratorEncoder, END_MARK);
    }

    public SecretKeySpec prepareSecreteKey(String baseSecretKey) throws SteganographyException {
        MessageDigest sha = null;
        try {
            byte[] key = baseSecretKey.getBytes(StandardCharsets.UTF_8);
            sha = MessageDigest.getInstance("SHA-1");
            key = sha.digest(key);
            key = Arrays.copyOf(key, KEY_LENGTH);
            return new SecretKeySpec(key, ALGORITHM);
        }
        catch (NoSuchAlgorithmException e) {
            throw new CipherException("");
        }
    }

    public String encrypt(String text, String secret) throws SteganographyException {
        try {
            SecretKeySpec secretKey = prepareSecreteKey(secret);
            Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            String markedText = appendEndMark(text);
            String encodedText = Base64.getEncoder().encodeToString(cipher.doFinal(markedText.getBytes(StandardCharsets.UTF_8)));
            return encryptWithDecorator(encodedText, secret);
        }
        catch (Exception e) {
            throw new CipherException("");
        }
    }

    public String decrypt(String encodedText, String secret) throws SteganographyException {
        try {
            encodedText = decryptWithDecorator(encodedText, secret);
            SecretKeySpec secretKey = prepareSecreteKey(secret);
            Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            String markedText = new String(cipher.doFinal(Base64.getDecoder().decode(encodedText)));
            if (!hasEndMarker(markedText))
                throw new SteganographyException("this file is not a steganography image file");
            return removeEndMark(markedText);
        }
        catch (Exception e) {
            throw new SteganographyException("this file is not a steganography image file");
        }
    }


}
