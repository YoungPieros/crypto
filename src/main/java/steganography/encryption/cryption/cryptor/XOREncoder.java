package steganography.encryption.cryption.cryptor;

import steganography.encryption.cryption.KeyBaseDecoratorEncoder;
import steganography.exception.SteganographyException;

public class XOREncoder extends KeyBaseDecoratorEncoder {

    private static final String END_MARK = "^^^^^^^";

    public XOREncoder() {
        super(null, END_MARK);
    }

    public XOREncoder(KeyBaseDecoratorEncoder decoratorEncoder) {
        super(decoratorEncoder, END_MARK);
    }

    @Override
    public String encrypt(String text, String secret) throws SteganographyException {
        String markedText = appendEndMark(text);
        String properSecretKey = createProperKey(markedText, secret);
        StringBuilder encodedText = new StringBuilder();
        for (int i = 0; i < markedText.length(); i++)
            encodedText.append((char)(markedText.charAt(i) ^ properSecretKey.charAt(i)));
        return encryptWithDecorator(encodedText.toString(), secret);
    }

    @Override
    public String decrypt(String encodedText, String secret) throws SteganographyException {
        encodedText = decryptWithDecorator(encodedText, secret);
        String properSecretKey = createProperKey(encodedText, secret);
        StringBuilder decodedText = new StringBuilder();
        for (int i = 0; i < encodedText.length(); i++)
            decodedText.append((char)(encodedText.charAt(i) ^ properSecretKey.charAt(i)));
        if (!hasEndMarker(decodedText.toString()))
            throw new SteganographyException("this file is not a steganography image file");
        return removeEndMark(decodedText.toString());
    }

    private String createProperKey (String text, String secret) {
        StringBuilder properKey = new StringBuilder(secret);
        while (properKey.length() < text.length())
            properKey.append(secret);
        return properKey.substring(0, text.length());
    }

}
