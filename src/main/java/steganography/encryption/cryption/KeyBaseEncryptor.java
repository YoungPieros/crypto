package steganography.encryption.cryption;


import steganography.encryption.cryption.cryptor.CipherEncoder;
import steganography.encryption.cryption.cryptor.XOREncoder;
import steganography.exception.SteganographyException;

public class KeyBaseEncryptor implements KeyBaseEncoder {

    private final KeyBaseDecoratorEncoder encoder;


    public KeyBaseEncryptor() {
        encoder = new CipherEncoder(new XOREncoder());
    }

    @Override
    public String encrypt(String text, String secret) throws SteganographyException {
        return encoder.encrypt(text, secret);
    }

    @Override
    public String decrypt(String encodedText, String secret) throws SteganographyException {
        return encoder.decrypt(encodedText, secret);
    }
}
