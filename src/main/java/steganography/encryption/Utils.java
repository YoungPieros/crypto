package steganography.encryption;

import java.util.HashMap;
import java.util.Map;

public class Utils {

    public static String addEndMark (String text, String endMark) {
        return text + endMark;
    }

    public static String removeEndMark (String text, String endMark) {
        if (text.endsWith(endMark))
            return text.substring(0, text.length() - endMark.length());
        return "";
    }

    public static boolean isEndMark (String text, String endMark) {
        return text.endsWith(endMark);
    }

    public static String getImageType (String contentType) {
        Map<String, String> formatMap = new HashMap<>();
        formatMap.put("bmp", "bmp");
        formatMap.put("png", "png");
        String fileType = contentType.split("/")[1];
        return formatMap.get(fileType);
    }

}
