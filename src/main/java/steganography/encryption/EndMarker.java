package steganography.encryption;

public interface EndMarker {

    public String removeEndMark (String encodedText);

    public String appendEndMark (String encodedText);

    public boolean hasEndMarker (String encodedText);

}
