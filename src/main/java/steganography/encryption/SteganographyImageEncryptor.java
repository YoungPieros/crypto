package steganography.encryption;

import steganography.SteganographyImageEncoder;
import steganography.encryption.cryption.KeyBaseEncryptor;
import steganography.encryption.steganography.SteganographyEncryptor;
import steganography.encryption.steganography.SteganographyEncryptorImpl;
import steganography.encryption.validator.*;
import steganography.exception.SteganographyException;
import domain.entity.SecurityKey;
import dto.SteganographyImageDto;

import javax.imageio.ImageIO;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class SteganographyImageEncryptor implements SteganographyImageEncoder {

    private final SecurityKey securityKey;
    private final SteganographyImageDto steganographyImageDto;
    private final KeyBaseEncryptor keyBaseEncryptor;
    private final SteganographyEncryptor steganographyEncryptor;

    public SteganographyImageEncryptor (SecurityKey securityKey, SteganographyImageDto steganographyImageDto) {
        this.steganographyImageDto = steganographyImageDto;
        this.securityKey = securityKey;
        this.keyBaseEncryptor = new KeyBaseEncryptor();
        this.steganographyEncryptor = new SteganographyEncryptorImpl();
    }


    @Override
    public void encrypt() throws SteganographyException {
        if (!SteganographyValidator.validatePureImage(steganographyImageDto))
            throw new SteganographyException("error in encrypting file");
        String encryptedText = keyBaseEncryptor.encrypt(steganographyImageDto.getText(), securityKey.getSecurityKey());
        steganographyEncryptor.encrypt(steganographyImageDto.getBufferedImage(), encryptedText, steganographyImageDto.getPassword());
    }

    @Override
    public void decrypt() throws SteganographyException {
        if (!SteganographyValidator.validateEncodedImage(steganographyImageDto))
            throw new SteganographyException("error in decrypting file");
        String hiddenText = steganographyEncryptor.decrypt(steganographyImageDto.getBufferedImage(), steganographyImageDto.getPassword());
        steganographyImageDto.setText(keyBaseEncryptor.decrypt(hiddenText, securityKey.getSecurityKey()));
    }

    public String getDecodedText () {
        return steganographyImageDto.getText();
    }

    public byte[] getEncodedImageBytes () {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        String imageStreamType = Utils.getImageType(steganographyImageDto.getContentType());
        try {
            ImageIO.write(steganographyImageDto.getBufferedImage(), imageStreamType, byteArrayOutputStream);
            return byteArrayOutputStream.toByteArray();
        }
        catch (IOException e) {
            return null;
        }
    }

}
