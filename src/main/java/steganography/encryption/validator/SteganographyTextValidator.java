package steganography.encryption.validator;

import steganography.exception.SteganographyException;
import dto.SteganographyImageDto;

public class SteganographyTextValidator implements SteganographyDataValidator {

    private final SteganographyDataValidator decoratedValidator;
    private final int TEXT_LIMIT_SIZE = 8192;

    public SteganographyTextValidator (SteganographyDataValidator decoratedValidator) {
        this.decoratedValidator = decoratedValidator;
    }

    @Override
    public boolean validate(SteganographyImageDto steganographyImageDto) throws SteganographyException {
        if (steganographyImageDto.getText() == null || steganographyImageDto.getText().length() == 0)
            throw new SteganographyException("secret text is empty");
        if (steganographyImageDto.getText().length() > TEXT_LIMIT_SIZE)
            throw new SteganographyException("secret text must be less than " + TEXT_LIMIT_SIZE + " character");
        if (decoratedValidator != null)
            return decoratedValidator.validate(steganographyImageDto);
        return true;
    }
}
