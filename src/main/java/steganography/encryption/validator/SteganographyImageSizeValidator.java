package steganography.encryption.validator;

import steganography.exception.SteganographyException;
import dto.SteganographyImageDto;

public class SteganographyImageSizeValidator implements SteganographyDataValidator {

    private final SteganographyDataValidator decoratedValidator;

    private final int MIN_WIDTH = 300;
    private final int MIN_HEIGHT = 400;

    public SteganographyImageSizeValidator(SteganographyDataValidator decoratedValidator) {
        this.decoratedValidator = decoratedValidator;
    }

    @Override
    public boolean validate(SteganographyImageDto steganographyImageDto) throws SteganographyException {
        int width = steganographyImageDto.getBufferedImage().getWidth();
        int height = steganographyImageDto.getBufferedImage().getHeight();
        if (width < MIN_WIDTH || height < MIN_HEIGHT)
            throw new SteganographyException("image size must be at least " + MIN_WIDTH + "(width) * " + MIN_HEIGHT + "(height)");
        if (decoratedValidator != null)
            return decoratedValidator.validate(steganographyImageDto);
        return true;
    }
}
