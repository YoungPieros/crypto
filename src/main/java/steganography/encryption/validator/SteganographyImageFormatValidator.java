package steganography.encryption.validator;

import steganography.exception.SteganographyException;
import dto.SteganographyImageDto;
import steganography.exception.UnsupportedImageFormat;
import web_services.Steganography;

import java.util.ArrayList;
import java.util.Arrays;

public class SteganographyImageFormatValidator implements SteganographyDataValidator {

    private final SteganographyDataValidator decoratedValidator;
    private final String contentType = "image";
    private final ArrayList<String> supportedFormats;

    public SteganographyImageFormatValidator (SteganographyDataValidator decoratedValidator) {
        this.decoratedValidator = decoratedValidator;
        supportedFormats = new ArrayList<>(Arrays.asList("png", "bmp"));
    }

    @Override
    public boolean validate(SteganographyImageDto steganographyImageDto) throws SteganographyException {
        String content = steganographyImageDto.getContentType();
        if (steganographyImageDto.getBufferedImage() == null)
            throw new SteganographyException("the file is not an image file");
        if (content == null)
            throw new SteganographyException("content file is missed");
        if (!content.contains(contentType))
            throw new SteganographyException("the file is not an image file");
        String[] contents = content.split("/");
        String format = contents.length > 1 ? contents[1] : "";
        if (format.equals(""))
            throw new UnsupportedImageFormat("file has unknown format");
        if (!supportedFormats.contains(format))
            throw new UnsupportedImageFormat("this format file is not supported");
        if (decoratedValidator != null)
            return decoratedValidator.validate(steganographyImageDto);
        return true;
    }
}
