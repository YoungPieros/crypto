package steganography.encryption.validator;

import dto.SteganographyImageDto;
import steganography.encryption.steganography.encryptor.SteganographyImage;
import steganography.exception.SteganographyException;

public class SteganographyValidator {

    public static boolean validateEncodedImage (SteganographyImageDto imageDto) throws SteganographyException {
        SteganographyDataValidator validator =
                new SteganographyImageSizeValidator(
                    new SteganographyImageFormatValidator(
                        new SteganographyPasswordValidator(null)));
        return validator.validate(imageDto);
    }

    public static boolean validatePureImage (SteganographyImageDto imageDto) throws SteganographyException {
        SteganographyDataValidator validator =
                new SteganographyTextValidator(
                    new SteganographyImageFormatValidator(
                        new SteganographyImageSizeValidator(
                                (new SteganographyPasswordValidator(null)))));
        return validator.validate(imageDto);
    }


}
