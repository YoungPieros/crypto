package steganography.encryption.validator;

import steganography.exception.SteganographyException;
import dto.SteganographyImageDto;

public interface SteganographyDataValidator {

    public boolean validate (SteganographyImageDto steganographyImageDto) throws SteganographyException;

}
