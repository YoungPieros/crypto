package steganography.encryption.validator;

import steganography.exception.SteganographyException;
import dto.SteganographyImageDto;

public class SteganographyPasswordValidator implements SteganographyDataValidator {

    private final SteganographyDataValidator decoratedValidator;
    private final int MIN_PASSWORD_LENGTH = 8;

    public SteganographyPasswordValidator (SteganographyDataValidator decoratedValidator) {
        this.decoratedValidator = decoratedValidator;
    }

    @Override
    public boolean validate(SteganographyImageDto steganographyImageDto) throws SteganographyException {
        if (steganographyImageDto.getPassword() == null)
            throw new SteganographyException("password is empty");
        if (steganographyImageDto.getPassword().length() < MIN_PASSWORD_LENGTH)
            throw new SteganographyException("password length must be greater than " + MIN_PASSWORD_LENGTH + " character");
        if (decoratedValidator != null)
            return decoratedValidator.validate(steganographyImageDto);
        return true;
    }
}
