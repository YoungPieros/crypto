package steganography.encryption.steganography.encryptor;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Random;

public class SteganographyImage {

    private BufferedImage bufferedImage;

    private int step = 0;
    private int blocksLength = 0;
    private int randomLsb = 0;
    public static final int BLOCK = 6;
    private final Point currentBlock = new Point();
    private final Point currentPoint = new Point();
    private final ArrayList<Integer> editedBlocks = new ArrayList<>();

    public SteganographyImage(BufferedImage bufferedImage) {
        this.bufferedImage = bufferedImage;
        blocksLength = calculateImageBlockLength();
    }

    public SteganographyImage(InputStream imageStream) {
        try {
            bufferedImage = ImageIO.read(imageStream);
            blocksLength = calculateImageBlockLength();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public BufferedImage getBufferedImage () {
        return bufferedImage;
    }

    public ArrayList<Integer> getEditedBlocks() {
        return editedBlocks;
    }

    public int getHeight () {
        return bufferedImage.getHeight();
    }

    public int getWidth () {
        return bufferedImage.getWidth();
    }

    public int getSize () {
        return bufferedImage.getHeight() * bufferedImage.getWidth();
    }

    public void setStep(int step) {
        this.step = step;
    }

    public Point getCurrentBlock() {
        return currentBlock;
    }

    public int getBlocksLength() {
        return blocksLength;
    }

    public Point getCurrentPoint() {
        return currentPoint;
    }

    public Color getCurrentPixelColor () {
        return new Color(bufferedImage.getRGB((int)currentPoint.getX(), (int)currentPoint.getY()));
    }

    private int calculateImageBlockLength () {
        return Math.floorDiv(bufferedImage.getHeight() * bufferedImage.getWidth(), BLOCK);
    }

    public int createLSB (String characterBytes, int characterCounter) {
        return characterCounter < characterBytes.length() ?
                Integer.parseInt(String.valueOf(characterBytes.charAt(characterCounter))) :
                Math.floorMod(randomLsb ^ Math.floorDiv(randomLsb++, 2), 2);
    }

    public void nextPoint () {
        int wideDistance = (int) (currentPoint.getX() * bufferedImage.getHeight() + currentPoint.getY());
        int nextDistance = wideDistance + 1;
        currentPoint.setLocation(Math.floorDiv(nextDistance, bufferedImage.getHeight()),
                Math.floorMod(nextDistance, bufferedImage.getHeight()));
    }

    public void nextBlock () {
        editedBlocks.add((int)((currentBlock.getX() * bufferedImage.getHeight() + currentBlock.getY()) / BLOCK));
        int point = Math.floorDiv((int) (currentBlock.getX() * bufferedImage.getHeight() + currentBlock.getY()),
                BLOCK);
        int wideDistance = Math.floorMod(step + point, blocksLength) * BLOCK;
        currentBlock.setLocation(Math.floorDiv(wideDistance, bufferedImage.getHeight()),
                Math.floorMod(wideDistance, bufferedImage.getHeight()));
        currentPoint.setLocation(currentBlock.getX(), currentBlock.getY());
    }

    public Color createEndMarkColor (Color color) {
        int redValue = 2 * Math.floorDiv(color.getRed(), 2) + Math.floorMod((new Random()).nextInt(), 2);
        int greenValue = 2 * Math.floorDiv(color.getGreen(), 2);
        int blueValue = 2 * Math.floorDiv(color.getBlue(), 2);
        return new Color(redValue, greenValue, blueValue);
    }

    public void colorizeCurrentPixel (Color color) {
        bufferedImage.setRGB((int)currentPoint.getX(), (int)currentPoint.getY(), color.getRGB());
    }

    public boolean isEndBlock() {
        int wideDistance = (int)(currentBlock.getX() * bufferedImage.getHeight() + currentBlock.getY() + BLOCK - 1);
        int x = Math.floorDiv(wideDistance, bufferedImage.getHeight());
        int y = Math.floorMod(wideDistance, bufferedImage.getHeight());
        Color pixel = new Color(bufferedImage.getRGB(x, y));
        return Math.floorMod(pixel.getGreen(), 2) == Math.floorMod(pixel.getBlue(), 2);
    }

}
