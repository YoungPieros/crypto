package steganography.encryption.steganography.encryptor;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.InputStream;

public class SteganographyDecoder extends SteganographyImage {


    public SteganographyDecoder(BufferedImage bufferedImage) {
        super(bufferedImage);
    }

    public SteganographyDecoder(InputStream imageStream) {
        super(imageStream);
    }

    public boolean hasNext () {
        return !isEndBlock();
    }

    public Character nextEmbeddedCharacter () {
        StringBuilder characterBytes = new StringBuilder();
        for (int i = 0; i < SteganographyImage.BLOCK; i++) {
            Color pixel = getCurrentPixelColor();
            characterBytes.append(Integer.toString(Math.floorMod(pixel.getRed(), 2)));
            characterBytes.append(Integer.toString(Math.floorMod(pixel.getGreen(), 2)));
            characterBytes.append(Integer.toString(Math.floorMod(pixel.getBlue(), 2)));
            nextPoint();
        }
        nextBlock();
        return Utils.extractCharacterFromBinaryString(characterBytes.toString());
    }

}
