package steganography.encryption.steganography.encryptor;

import java.awt.*;
import java.util.Random;

public class Utils {

    public static String getByteCharacter (Character character) {
        String stringBytes = Integer.toBinaryString(character);
        stringBytes = "0".repeat(Character.SIZE - stringBytes.length()) + stringBytes;
        return stringBytes;
    }

    public static Character extractCharacterFromBinaryString (String binary) {
        binary = binary.substring(0, Character.SIZE);
        int integerValue = Integer.parseUnsignedInt(binary, 2);
        return (char)integerValue;
    }

    public static Color createNoiseColor (Color color) {
        Random random = new Random();
        int redValue = 2 * Math.floorDiv(color.getRed(), 2) + Math.floorMod(random.nextInt(), 2);
        int greenValue = 2 * Math.floorDiv(color.getGreen(), 2) + Math.floorMod(random.nextInt(), 2);
        int blueValue = 2 * Math.floorDiv(color.getBlue(), 2) + Math.floorMod(random.nextInt(), 2);
        return new Color(redValue, greenValue, blueValue);
    }


}
