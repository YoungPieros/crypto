package steganography.encryption.steganography.encryptor;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.Collections;

public class SteganographyEncoder extends SteganographyImage {

    public SteganographyEncoder(BufferedImage bufferedImage) {
        super(bufferedImage);
    }

    public SteganographyEncoder(InputStream imageStream) {
        super(imageStream);
    }

    public void embedCharInImage (Character character) {
        String characterBytes = Utils.getByteCharacter(character);
        for (int i = 0; i < BLOCK; i++) {
            Color color = getCurrentPixelColor();
            Color editedColor = editRGBValue(color, characterBytes, 3 * i);
            getBufferedImage().setRGB((int)getCurrentPoint().getX(), (int)getCurrentPoint().getY(), editedColor.getRGB());
            nextPoint();
        }
        nextBlock();
    }

    private Color editRGBValue (Color color, String characterBytes, int characterCounter) {
        int redValue = 2 * Math.floorDiv(color.getRed(), 2) + createLSB(characterBytes, characterCounter++);
        int greenValue = 2 * Math.floorDiv(color.getGreen(), 2) + createLSB(characterBytes, characterCounter++);
        int blueValue = 2 * Math.floorDiv(color.getBlue(), 2) + createLSB(characterBytes, characterCounter);
        return new Color(redValue, greenValue, blueValue);
    }

    public void setEndMark () {
        for (int i = 0; i < BLOCK - 1; i++) {
            Color colorOfCurrentPoint = getCurrentPixelColor();
            Color noiseColor = Utils.createNoiseColor(colorOfCurrentPoint);
            colorizeCurrentPixel(noiseColor);
            nextPoint();
        }
        Color markPixelColor = createEndMarkColor(getCurrentPixelColor());
        colorizeCurrentPixel(markPixelColor);
        nextBlock();
    }

    public void makeNoise () {
        ArrayList<Integer> blocks = new ArrayList<>(getEditedBlocks());
        Collections.sort(blocks);
        blocks.add(getSize() + 1);
        int blockCounter = 0;
        BufferedImage bufferedImage = getBufferedImage();
        for (int i = 0; i < bufferedImage.getWidth(); i++)
            for (int j = 0; j < bufferedImage.getHeight(); j++) {
                int blockNumber = Math.floorDiv(i * bufferedImage.getHeight() + j, BLOCK);
                if (blockNumber == blocks.get((int)(blockCounter / BLOCK))) {
                    blockCounter++;
                    continue;
                }
                Color noiseColor = Utils.createNoiseColor(new Color(bufferedImage.getRGB(i, j)));
                bufferedImage.setRGB(i, j, noiseColor.getRGB());
            }
    }

}
