package steganography.encryption.steganography;

import steganography.exception.NotSteganographyImage;
import steganography.exception.SteganographyException;

import java.awt.image.BufferedImage;

public interface SteganographyEncryptor  {


    public void encrypt (BufferedImage bufferedImage, String text, String password);

    public String decrypt (BufferedImage bufferedImage, String password) throws NotSteganographyImage, SteganographyException;

}
