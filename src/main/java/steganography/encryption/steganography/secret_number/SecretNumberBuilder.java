package steganography.encryption.steganography.secret_number;

public interface SecretNumberBuilder {

    public int generateNumber(String secretKey, int downLimit);

}
