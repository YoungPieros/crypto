package steganography.encryption.steganography.secret_number;

import java.util.ArrayList;

public class SecretNumberGenerator implements SecretNumberBuilder {


    private ArrayList<Integer> primes;
    private static final int DOWN_LIMIT_COEFFICIENT = 4;
    private static final int SECRET_KEY_UP_RANGE_COEFFICIENT = 3;
    private static SecretNumberGenerator secretNumberGenerator;

    public static SecretNumberGenerator getInstance() {
        if (secretNumberGenerator == null) {
            synchronized (SecretNumberGenerator.class) {
                if (secretNumberGenerator == null)
                    secretNumberGenerator = new SecretNumberGenerator();
            }
        }
        return secretNumberGenerator;
    }

    private SecretNumberGenerator() {
        primes = new ArrayList<>();
        primes.add(2);
    }

    @Override
    public int generateNumber(String secretKey, int downLimit) {
        if (needToExtend(downLimit))
            extendPrimeNumbers(downLimit);
        return chooseSecretNumber(secretKey, downLimit);
    }

    private boolean needToExtend(int downLimit) {
        return downLimit * DOWN_LIMIT_COEFFICIENT > maximumPrime();
    }

    private int maximumPrime() {
        return primes.get(primes.size() - 1);
    }

    private synchronized void extendPrimeNumbers(int downLimit) {
        if (!needToExtend(downLimit))
            return;
        downLimit = downLimit * DOWN_LIMIT_COEFFICIENT;
        for (int number = maximumPrime() + 1; number <= downLimit; number++)
            if (isPrime(number))
                primes.add(number);
    }

    private boolean isPrime(int number) {
        for (Integer prime : primes)
            if (number < Math.pow(prime, 2))
                return true;
            else if (Math.floorMod(number, prime) == 0)
                return false;
        return true;
    }

    private int chooseSecretNumber(String secretKey, int downLimit) {
        int baseRange = nextIndexAfterNumber(downLimit);
        int endRange = nextIndexBeforeNumber(downLimit * SECRET_KEY_UP_RANGE_COEFFICIENT);
        int offset = Math.floorMod(secretKey.hashCode(), endRange - baseRange);
        return primes.get(baseRange + offset);
    }

    private int nextIndexAfterNumber (int number) {
        int index;
        int prime;
        int firstIndexRange = 0;
        int lastIndexRange = primes.size() - 1;

        while (firstIndexRange < lastIndexRange) {
            index = Math.floorDiv(firstIndexRange + lastIndexRange, 2);
            prime = primes.get(index);
            if (prime > number)
                lastIndexRange = index;
            else
                firstIndexRange = index + 1;
        }

        return lastIndexRange;
    }

    private int nextIndexBeforeNumber (int number) {
        int index;
        int prime;
        int firstIndexRange = 0;
        int lastIndexRange = primes.size() - 1;

        while (firstIndexRange < lastIndexRange) {
            index = Math.floorDiv(firstIndexRange + lastIndexRange + 1, 2);
            prime = primes.get(index);
            if (prime > number)
                lastIndexRange = index - 1;
            else
                firstIndexRange = index;
        }
        return firstIndexRange;
    }

    public ArrayList<Integer> getPrimes() {
        return primes;
    }
}