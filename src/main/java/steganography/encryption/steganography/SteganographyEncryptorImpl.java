package steganography.encryption.steganography;

import steganography.encryption.EndMarker;
import steganography.encryption.Utils;
import steganography.encryption.steganography.encryptor.SteganographyDecoder;
import steganography.encryption.steganography.encryptor.SteganographyEncoder;
import steganography.encryption.steganography.secret_number.SecretNumberGenerator;
import steganography.exception.NotSteganographyImage;
import steganography.exception.SteganographyException;

import java.awt.image.BufferedImage;

public class SteganographyEncryptorImpl implements SteganographyEncryptor, EndMarker {

    private final String endMark = "@@@@@@@";


    public SteganographyEncryptorImpl() {
    }

    @Override
    public void encrypt(BufferedImage bufferedImage, String text, String password) {
        SteganographyEncoder imageProcessorEncoder = new SteganographyEncoder(bufferedImage);
        String markedText = appendEndMark(text);
        int step = SecretNumberGenerator.getInstance().generateNumber(password, imageProcessorEncoder.getSize());
        imageProcessorEncoder.setStep(step);
        for (int i = 0; i < markedText.length(); i++)
            imageProcessorEncoder.embedCharInImage(markedText.charAt(i));
        imageProcessorEncoder.setEndMark();
        imageProcessorEncoder.makeNoise();
    }

    @Override
    public String decrypt(BufferedImage bufferedImage, String password) throws SteganographyException {
        SteganographyDecoder imageProcessorDecoder = new SteganographyDecoder(bufferedImage);
        int step = SecretNumberGenerator.getInstance().generateNumber(password, imageProcessorDecoder.getSize());
        imageProcessorDecoder.setStep(step);
        StringBuilder decodedMarkedText = new StringBuilder();
        while (imageProcessorDecoder.hasNext()) {
            decodedMarkedText.append(imageProcessorDecoder.nextEmbeddedCharacter());
            if (decodedMarkedText.length() > imageProcessorDecoder.getBlocksLength())
                throw new NotSteganographyImage("bad steganography file");
        }
        if (!hasEndMarker(decodedMarkedText.toString()))
            throw new NotSteganographyImage("bad steganography file");
        return removeEndMark(decodedMarkedText.toString());
    }

    @Override
    public String removeEndMark(String encodedText) {
        return Utils.removeEndMark(encodedText, endMark);
    }

    @Override
    public String appendEndMark(String encodedText) {
        return Utils.addEndMark(encodedText, endMark);
    }

    @Override
    public boolean hasEndMarker (String encodedText) {
        return Utils.isEndMark(encodedText, endMark);
    }

}
