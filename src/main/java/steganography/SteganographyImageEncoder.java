package steganography;

import steganography.exception.SteganographyException;

public interface SteganographyImageEncoder {

    public void encrypt () throws SteganographyException;

    public void decrypt () throws SteganographyException;

    public String getDecodedText ();

    public byte[] getEncodedImageBytes ();

}
