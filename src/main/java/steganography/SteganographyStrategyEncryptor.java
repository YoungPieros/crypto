package steganography;

import domain.entity.SecurityKey;
import dto.SteganographyImageDto;
import steganography.encryption.SteganographyImageEncryptor;
import steganography.exception.SteganographyException;

public class SteganographyStrategyEncryptor implements SteganographyImageEncoder {

    private final SteganographyImageEncryptor encryptor;

    public SteganographyStrategyEncryptor (SecurityKey securityKey, SteganographyImageDto steganographyImageDto) {
        encryptor = new SteganographyImageEncryptor(securityKey, steganographyImageDto);
    }

    public static SteganographyImageEncoder createEncryptor (SecurityKey securityKey, SteganographyImageDto imageDto) {
        return new SteganographyImageEncryptor(securityKey, imageDto);
    }

    @Override
    public void encrypt() throws SteganographyException {
        encryptor.encrypt();
    }

    @Override
    public void decrypt() throws SteganographyException {
        encryptor.decrypt();
    }

    public byte[] getEncodedImageBytes () {
        return encryptor.getEncodedImageBytes();
    }

    public String getDecodedText () {
        return encryptor.getDecodedText();
    }

}
