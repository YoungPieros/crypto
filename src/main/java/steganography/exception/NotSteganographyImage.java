package steganography.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "file is not steganography image")
public class NotSteganographyImage extends SteganographyException {

    public NotSteganographyImage (String message) {
        super(message);
    }

}
