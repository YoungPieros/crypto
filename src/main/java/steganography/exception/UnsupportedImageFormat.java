package steganography.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "this type is can not be supported and hold encoded data")
public class UnsupportedImageFormat extends SteganographyException {

    public UnsupportedImageFormat(String message) {
        super(message);
    }

}
