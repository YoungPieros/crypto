package steganography.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "file is not steganography image")
public class CipherException extends SteganographyException {

    public CipherException(String message) {
        super(message);
    }

}
