package repository;

import domain.entity.SecurityKey;

public interface SecurityKeyRepository {

    public void save(SecurityKey securityKey);

    public SecurityKey load(String userId);

}
