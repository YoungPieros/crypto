package repository.sql;

import domain.entity.User;
import org.hibernate.Session;
import repository.UserRepository;
import repository.sql.base.BaseRepositorySQL;
import repository.sql.base.SQLSession;

public class UserRepositorySQL implements UserRepository {

    private static UserRepositorySQL repository;

    private UserRepositorySQL() {
    }

    public static UserRepositorySQL getInstance () {
        if (repository == null) {
            synchronized (UserRepositorySQL.class) {
                repository = new UserRepositorySQL();
            }
        }
        return repository;
    }

    @Override
    public void saveUser(User user) {
        BaseRepositorySQL.getInstance().save(user);
    }

    @Override
    public boolean existsEmail(String email) {
        return getUserByEmail(email) != null;
    }

    @Override
    public User getUserByEmail(String email) {
        try {
            return BaseRepositorySQL.getInstance().loadWithValue(User.class, "email", email);
        }
        catch (Exception exception) {
            return null;
        }
    }

    @Override
    public User load (String id) {
        Session session = null;
        try {
            session = SQLSession.getInstance().getSessionFactory().openSession();
            return BaseRepositorySQL.getInstance().load(User.class, "id", id);
        }
        catch (Exception e) {
            return null;
        }
        finally {
            if (session != null)
                session.close();
        }
    }

    @Override
    public void deleteUser(String id) {
        User user = load(id);
        BaseRepositorySQL.getInstance().delete(user);
    }

}
