package repository.sql;

import domain.entity.SecurityKey;
import repository.SecurityKeyRepository;
import repository.sql.base.BaseRepositorySQL;

public class SecurityKeyRepositorySQL implements SecurityKeyRepository {

    private static SecurityKeyRepositorySQL repository;

    private SecurityKeyRepositorySQL() {
    }

    public static SecurityKeyRepositorySQL getInstance () {
        if (repository == null) {
            synchronized (SecurityKeyRepositorySQL.class) {
                repository = new SecurityKeyRepositorySQL();
            }
        }
        return repository;
    }


    @Override
    public void save(SecurityKey securityKey) {
        BaseRepositorySQL.getInstance().save(securityKey);
    }

    @Override
    public SecurityKey load(String userId) {
        return BaseRepositorySQL.getInstance().load(SecurityKey.class, "userId", userId);
    }
}
