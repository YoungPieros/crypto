package repository.sql;

import domain.entity.User;
import domain.entity.UserAuthority;
import org.hibernate.Session;
import repository.UserAuthorityRepository;
import repository.sql.base.BaseRepositorySQL;
import repository.sql.base.SQLSession;

public class UserAuthorityRepositorySQL implements UserAuthorityRepository {

    private static UserAuthorityRepositorySQL repository;

    private UserAuthorityRepositorySQL() {
    }

    public static UserAuthorityRepositorySQL getInstance() {
        if (repository == null) {
            synchronized (UserAuthorityRepositorySQL.class) {
                if (repository == null)
                    repository = new UserAuthorityRepositorySQL();
            }
        }
        return repository;
    }

    @Override
    public UserAuthority getUserAuthorityById (String id) {
        Session session = null;
        try {
            session = SQLSession.getInstance().getSessionFactory().openSession();
            return BaseRepositorySQL.getInstance().load(UserAuthority.class, "userId", id);
        }
        catch (Exception e) {
            return null;
        }
        finally {
            if (session != null)
                session.close();
        }
    }

    @Override
    public UserAuthority getUserAuthority(String email) {
        try {
            return BaseRepositorySQL.getInstance().loadWithValue(UserAuthority.class, "email", email);
        }
        catch (Exception exception) {
            return null;
        }
    }

    @Override
    public void save(UserAuthority userAuthority) {
        BaseRepositorySQL.getInstance().save(userAuthority);
    }

    @Override
    public void delete(UserAuthority userAuthority) {
        BaseRepositorySQL.getInstance().delete(userAuthority);
    }
}
