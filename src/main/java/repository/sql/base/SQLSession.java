package repository.sql.base;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class SQLSession {

    private final SessionFactory sessionFactory;

    private static SQLSession sessionSQL;

    private SQLSession() {
        StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
        Metadata metadata = new MetadataSources(ssr).getMetadataBuilder().build();
        sessionFactory = metadata.getSessionFactoryBuilder().build();
    }

    public static SQLSession getInstance() {
        if (sessionSQL == null) {
            synchronized (SQLSession.class) {
                if (sessionSQL == null)
                    sessionSQL = new SQLSession();
            }
        }
        return sessionSQL;
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

}
