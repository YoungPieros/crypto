package repository.sql.base;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class BaseRepositorySQL {

    private static BaseRepositorySQL repository;
    private final SessionFactory sessionFactory;

    private BaseRepositorySQL() {
        sessionFactory = SQLSession.getInstance().getSessionFactory();
    }

    public static BaseRepositorySQL getInstance () {
        if (repository == null) {
            synchronized (BaseRepositorySQL.class) {
                repository = new BaseRepositorySQL();
            }
        }
        return repository;
    }


    public <T> T load (Class<T> t, String fieldName, Object fieldValue) {
        Session session = sessionFactory.openSession();
        try {
            return loadWithValue(t, fieldName, fieldValue);
        }
        catch (Exception e) {
            return null;
        }
        finally {
            if (session != null)
                session.close();
        }
    }

    public <T> T loadWithValue (Class<T> t, String fieldName, Object fieldValue) throws Exception {
        Session session = sessionFactory.openSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<T> query = criteriaBuilder.createQuery(t);
        Root<T> root = query.from(t);
        query.select(root).where(criteriaBuilder.equal(root.get(fieldName), fieldValue));
        Query result = session.createQuery(query);
        return  (T) result.getSingleResult();
    }

    public void save (Object object) {
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();
            session.save(object);
            transaction.commit();
        }
        finally {
            if (session != null)
                session.close();
        }
    }

    public void delete (Object object) {
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();
            session.remove(object);
            transaction.commit();
        }
        finally {
            if (session != null)
                session.close();
        }
    }


}
