package repository.sql;

import domain.entity.UnregisterUser;
import repository.UnregisterAccountRepository;
import repository.sql.base.BaseRepositorySQL;

public class UnregisterAccountRepositorySQL implements UnregisterAccountRepository {

    private static UnregisterAccountRepositorySQL repository;

    private UnregisterAccountRepositorySQL() {
    }

    public static UnregisterAccountRepositorySQL getInstance() {
        if (repository == null) {
            synchronized (UnregisterAccountRepositorySQL.class) {
                if (repository == null)
                    repository = new UnregisterAccountRepositorySQL();
            }
        }
        return repository;
    }

    @Override
    public void saveUnregisterUser(UnregisterUser unregisterUser) {
        BaseRepositorySQL.getInstance().save(unregisterUser);
    }

    @Override
    public UnregisterUser loadUnregisterUser (String token) {
        return BaseRepositorySQL.getInstance().load(UnregisterUser.class, "registerToken", token);
    }

    @Override
    public boolean existsEmail (String email) {
        return BaseRepositorySQL.getInstance().load(UnregisterUser.class, "email", email) != null;
    }

    @Override
    public void deleteUnregisterUser(UnregisterUser unregisterUser) {
        BaseRepositorySQL.getInstance().delete(unregisterUser);
    }
}
