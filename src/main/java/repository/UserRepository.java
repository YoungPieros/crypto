package repository;

import domain.entity.User;

public interface UserRepository {

    public boolean existsEmail (String email);

    public User getUserByEmail (String email);

    public void saveUser (User user);

    public User load (String id);

    public void deleteUser (String id);

}
