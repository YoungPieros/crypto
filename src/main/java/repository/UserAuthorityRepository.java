package repository;

import domain.entity.UserAuthority;

public interface UserAuthorityRepository {

    public UserAuthority getUserAuthorityById (String id);

    public UserAuthority getUserAuthority (String email);

    public void save (UserAuthority userAuthority);

    public void delete (UserAuthority userAuthority);

}
