package repository.proxy;

import domain.entity.UserAuthority;
import repository.UserAuthorityRepository;
import repository.sql.UserAuthorityRepositorySQL;

public class ProxyUserAuthorityRepository implements UserAuthorityRepository {

    private static UserAuthorityRepository proxyRepository;
    private final UserAuthorityRepositorySQL repository;

    private ProxyUserAuthorityRepository() {
        repository = UserAuthorityRepositorySQL.getInstance();
    }

    public static UserAuthorityRepository getRepository () {
        if (proxyRepository == null) {
            synchronized (ProxyUserAuthorityRepository.class) {
                proxyRepository = new ProxyUserAuthorityRepository();
            }
        }
        return proxyRepository;
    }

    @Override
    public UserAuthority getUserAuthorityById(String id) {
        return repository.getUserAuthorityById(id);
    }

    @Override
    public UserAuthority getUserAuthority(String email) {
        return repository.getUserAuthority(email);
    }

    @Override
    public void save(UserAuthority userAuthority) {
        repository.save(userAuthority);
    }

    @Override
    public void delete(UserAuthority userAuthority) {
        repository.delete(userAuthority);
    }
}
