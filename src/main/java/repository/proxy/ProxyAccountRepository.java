package repository.proxy;

import domain.entity.User;
import repository.UserRepository;
import repository.sql.UserRepositorySQL;

public class ProxyAccountRepository implements UserRepository {

    private static UserRepository proxyRepository;
    private final UserRepositorySQL repository;

    private ProxyAccountRepository() {
        repository = UserRepositorySQL.getInstance();
    }

    public static UserRepository getRepository () {
        if (proxyRepository == null) {
            synchronized (ProxyAccountRepository.class) {
                proxyRepository = new ProxyAccountRepository();
            }
        }
        return proxyRepository;
    }

    @Override
    public boolean existsEmail(String email) {
        return repository.existsEmail(email);
    }

    @Override
    public User getUserByEmail(String email) {
        return repository.getUserByEmail(email);
    }

    @Override
    public void saveUser(User user) {
        repository.saveUser(user);
    }

    @Override
    public User load(String id) {
        return repository.load(id);
    }

    @Override
    public void deleteUser(String id) {
        repository.deleteUser(id);
    }
}
