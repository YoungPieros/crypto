package repository.proxy;

import domain.entity.SecurityKey;
import repository.SecurityKeyRepository;
import repository.sql.SecurityKeyRepositorySQL;

public class ProxySecurityRepository implements SecurityKeyRepository {

    private static SecurityKeyRepository proxyRepository;
    private final SecurityKeyRepositorySQL repository;

    private ProxySecurityRepository() {
        repository = SecurityKeyRepositorySQL.getInstance();
    }

    public static SecurityKeyRepository getRepository () {
        if (proxyRepository == null) {
            synchronized (ProxySecurityRepository.class) {
                proxyRepository = new ProxySecurityRepository();
            }
        }
        return proxyRepository;
    }


    @Override
    public void save(SecurityKey securityKey) {
        repository.save(securityKey);
    }

    @Override
    public SecurityKey load(String userId) {
        return repository.load(userId);
    }
}
