package repository.proxy;

import domain.entity.UnregisterUser;
import repository.UnregisterAccountRepository;
import repository.sql.UnregisterAccountRepositorySQL;

public class ProxyUnregisterAccountRepository implements UnregisterAccountRepository {

    private static UnregisterAccountRepository proxyRepository;
    private final UnregisterAccountRepositorySQL repository;

    private ProxyUnregisterAccountRepository() {
        repository = UnregisterAccountRepositorySQL.getInstance();
    }

    public static UnregisterAccountRepository getRepository () {
        if (proxyRepository == null) {
            synchronized (ProxyUnregisterAccountRepository.class) {
                proxyRepository = new ProxyUnregisterAccountRepository();
            }
        }
        return proxyRepository;
    }


    @Override
    public void saveUnregisterUser(UnregisterUser unregisterUser) {
        repository.saveUnregisterUser(unregisterUser);
    }

    @Override
    public UnregisterUser loadUnregisterUser(String token) {
        return repository.loadUnregisterUser(token);
    }

    @Override
    public boolean existsEmail(String email) {
        return repository.existsEmail(email);
    }

    @Override
    public void deleteUnregisterUser(UnregisterUser unregisterUser) {
        repository.deleteUnregisterUser(unregisterUser);
    }
}
