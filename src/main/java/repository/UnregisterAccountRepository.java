package repository;

import domain.entity.UnregisterUser;

public interface UnregisterAccountRepository {

    public void saveUnregisterUser (UnregisterUser unregisterUser);

    public UnregisterUser loadUnregisterUser (String token);

    public boolean existsEmail (String email);

    public void deleteUnregisterUser (UnregisterUser unregisterUser);

}
