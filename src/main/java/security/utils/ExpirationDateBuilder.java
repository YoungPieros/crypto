package security.utils;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

public class ExpirationDateBuilder {

    public static Date createExpirationDate (int expiryTimeInMinute) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Timestamp(calendar.getTime().getTime()));
        calendar.add(Calendar.MINUTE, expiryTimeInMinute);
        Date expiredDate = new Date(calendar.getTime().getTime());
        return expiredDate;
    }

}
