package security.password;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class PasswordEncoder {

    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private final int STRENGTH = 31; // strength value must be greater than 3 and less than 32 or equal -1

    public PasswordEncoder () {
        bCryptPasswordEncoder = new BCryptPasswordEncoder(STRENGTH);
    }

    public String encode (String password) {
        return bCryptPasswordEncoder.encode(password);
    }

}
