package security.authentication.jwt;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import org.springframework.security.authentication.*;

@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint {


    @Override
    public void commence(HttpServletRequest httpServletRequest,
                         HttpServletResponse httpServletResponse,
                         AuthenticationException e) throws IOException, ServletException {
        Exception exception = (Exception) httpServletRequest.getAttribute("exception");
        if (exception == null)
            httpServletResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "bad credential");
        else if (exception instanceof AuthenticationCredentialsNotFoundException)
            httpServletResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, exception.getMessage());
        else if (exception instanceof CredentialsExpiredException)
            httpServletResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, exception.getMessage());
        else if (exception instanceof BadCredentialsException)
            httpServletResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, exception.getMessage());
        else
            httpServletResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "bad credential");
    }
}
