package security.authentication.jwt;

import com.auth0.jwt.algorithms.Algorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
@Service
public class ApplicationJwt {

    public final static String bearer = "Bearer";

    @Value("${jwt.secret}")
    private String secret;
    @Value("${jwt.issuer}")
    private String ISSUER;
    @Value("${jwt.validityTime}")
    private long tokenValidity;

    private Algorithm algorithm;


    public Algorithm getAlgorithm() {
        if (algorithm == null)
            algorithm = Algorithm.HMAC512(secret);
        return algorithm;
    }

    public String getISSUER() {
        return ISSUER;
    }

    public long getTokenValidity() {
        return tokenValidity;
    }
}
