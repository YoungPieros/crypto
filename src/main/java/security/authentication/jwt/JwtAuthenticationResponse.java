package security.authentication.jwt;

public class JwtAuthenticationResponse {

    private String token;
    private String type = ApplicationJwt.bearer;

    public JwtAuthenticationResponse(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public String getType() {
        return type;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setType(String type) {
        this.type = type;
    }
}
