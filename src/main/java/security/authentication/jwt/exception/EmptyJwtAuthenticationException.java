package security.authentication.jwt.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.ServletException;

@ResponseStatus(code = HttpStatus.UNAUTHORIZED, reason = "empty jwt authentication")
public class EmptyJwtAuthenticationException extends ServletException {

    public EmptyJwtAuthenticationException(String message) {
        super(message);
    }
}
