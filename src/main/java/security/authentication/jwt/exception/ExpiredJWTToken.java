package security.authentication.jwt.exception;

import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.UNAUTHORIZED, reason = "session is expired")
public class ExpiredJWTToken extends CredentialsExpiredException {

    public ExpiredJWTToken(String message) {
        super(message);
    }
}
