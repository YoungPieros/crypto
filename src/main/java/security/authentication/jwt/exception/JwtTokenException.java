package security.authentication.jwt.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.ServletException;

@ResponseStatus(code = HttpStatus.UNAUTHORIZED)
public class JwtTokenException extends ServletException {

    public JwtTokenException (String message) {
        super(message);
    }

}
