package security.authentication.jwt;

import domain.entity.SecurityKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Service;
import org.springframework.web.filter.OncePerRequestFilter;
import security.authentication.AuthenticationManager;
import security.authentication.UserAuthorityDetailService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Service
public class JwtAuthenticationFilter extends OncePerRequestFilter    {

    @Autowired
    private JwtVerifier verifier;

    public JwtAuthenticationFilter (AuthenticationManager authenticationManager) {
    }



    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        String jwtToken = extractJtwToken(httpServletRequest);
        try {
            if (jwtToken == null)
                throw new AuthenticationCredentialsNotFoundException("authentication token is needed");
            verifier.verify(jwtToken);
        }
        catch (AuthenticationException exception) {
            httpServletRequest.setAttribute("exception", exception);
            filterChain.doFilter(httpServletRequest, httpServletResponse);
            return;
        }
        String id = verifier.getId(jwtToken);
        UserDetails userAuthorityDetails = (new UserAuthorityDetailService()).loadUserById(id);
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(userAuthorityDetails, null, null);
        authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }

    private String extractJtwToken (HttpServletRequest request) {
        String bearerToken = request.getHeader("Authorization");
        if (bearerToken == null)
            return null;
        if (bearerToken.startsWith("Bearer "))
            return bearerToken.substring(7, bearerToken.length());
        return null;
    }
}
