package security.authentication.jwt;

import com.auth0.jwt.JWT;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
@Service
public class JwtBuilder {

    @Autowired
    private ApplicationJwt applicationJwt;

    private final Map<String, Object> header;

    public JwtBuilder () {
        header = new HashMap<>();
        header.put("type", "JWT");
        header.put("alg", "HS512");
    }

    public String createToken (String userId) {
        long currentTime = (new Date()).getTime() / 1000;
        return JWT.create()
                .withHeader(header)
                .withIssuer(applicationJwt.getISSUER())
                .withClaim("id", userId)
                .withClaim("iat", currentTime)
                .withClaim("exp", currentTime + applicationJwt.getTokenValidity())
                .sign(applicationJwt.getAlgorithm());
    }




}
