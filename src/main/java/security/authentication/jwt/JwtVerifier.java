package security.authentication.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import repository.proxy.ProxyAccountRepository;

import java.util.Map;

@Component
@Service
public class JwtVerifier {

    @Autowired
    private ApplicationJwt applicationJwt;
    private JWTVerifier JWTverifier;

    public boolean verify (String token) throws AuthenticationException {
        JWTverifier = JWT.require(applicationJwt.getAlgorithm())
                .withIssuer(applicationJwt.getISSUER())
                .build();
        try {
            JWTverifier.verify(token);
        }
        catch (TokenExpiredException e) {
            throw new CredentialsExpiredException(e.getMessage());
        }
        catch (JWTVerificationException e) {
            throw new BadCredentialsException("bad credential token");
        }
        return true;
    }

    public String getId (String token) {
        String id;
        try {
            DecodedJWT jwt = JWTverifier.verify(token);
            Map<String, Claim> claims = jwt.getClaims();
            id = claims.get("id").asString();
            if (ProxyAccountRepository.getRepository().load(id) == null)
                return null;
        }
        catch (JWTDecodeException e) {
            return null;
        }
        return id;
    }


}
