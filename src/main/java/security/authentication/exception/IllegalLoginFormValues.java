package security.authentication.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "invalid login parameters")
public class IllegalLoginFormValues extends IllegalLoginArgument {
    public IllegalLoginFormValues(String msg, Throwable cause) {
        super(msg, cause);
    }
}
