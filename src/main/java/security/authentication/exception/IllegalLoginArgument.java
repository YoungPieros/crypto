package security.authentication.exception;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "invalid login parameters")
public class IllegalLoginArgument extends AuthenticationException {

    public IllegalLoginArgument(String msg, Throwable cause) {
        super(msg, cause);
    }

}
