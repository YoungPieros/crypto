package security.authentication.validator;

import dto.UserAccount;
import security.registeration.validator.ArgumentValidator;
import security.authentication.exception.IllegalLoginArgument;

public abstract class LoginArgumentValidator extends ArgumentValidator {

    public abstract boolean validate (UserAccount userAccount) throws IllegalLoginArgument;

}
