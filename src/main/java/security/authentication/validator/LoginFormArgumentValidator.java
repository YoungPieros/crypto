package security.authentication.validator;

import dto.UserAccount;
import security.authentication.exception.IllegalLoginArgument;
import security.authentication.exception.IllegalLoginFormValues;

public class LoginFormArgumentValidator extends LoginArgumentValidator {

    private LoginArgumentValidator decoratorValidator;

    public LoginFormArgumentValidator () {

    }

    public LoginFormArgumentValidator (LoginArgumentValidator decoratorValidator) {
        this.decoratorValidator = decoratorValidator;
    }

    @Override
    public boolean validate(UserAccount userAccount) throws IllegalLoginArgument {
        if (isArgumentEmpty(userAccount.getEmail()))
            throw new IllegalLoginFormValues("", new Throwable());
        if (isArgumentEmpty(userAccount.getPassword()))
            throw new IllegalLoginFormValues("", new Throwable());
        if (decoratorValidator != null)
            return decoratorValidator.validate(userAccount);
        return true;
    }
}
