package security.authentication;

import domain.entity.UserAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import repository.proxy.ProxyUserAuthorityRepository;


@Component
public class UserAuthorityDetailService implements UserDetailsService {

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        UserAuthority userAuthority = ProxyUserAuthorityRepository.getRepository().getUserAuthority(email);
        if (userAuthority == null)
            throw new UsernameNotFoundException("email: \"" + email + "\" not found");
        return UserAuthorityDetails.create(userAuthority);
    }

    public UserDetails loadUserById (String id) throws UsernameNotFoundException {
        UserAuthority userAuthority = ProxyUserAuthorityRepository.getRepository().getUserAuthorityById(id);
        if (userAuthority == null)
            throw new UsernameNotFoundException("");
        return UserAuthorityDetails.create(userAuthority);
    }

}
