package security.authentication;

import domain.entity.UserAuthority;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

public class UserAuthorityDetails extends UserAuthority implements UserDetails {

    public UserAuthorityDetails (String id, String email, String password) {
        super();
        super.setEmail(email);
        super.setUserId(id);
        super.setPassword(password);
    }

    public static UserAuthorityDetails create(UserAuthority authority) {
        return new UserAuthorityDetails(authority.getUserId(),
                                        authority.getEmail(),
                                        authority.getPassword());
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getUsername() {
        return getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
