package security.authentication;

import domain.entity.UserAuthority;
import dto.UserAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Service;
import repository.proxy.ProxyUserAuthorityRepository;
import security.authentication.exception.IllegalLoginArgument;
import security.authentication.exception.WrongCredential;
import security.authentication.jwt.JwtBuilder;
import security.authentication.validator.LoginArgumentValidator;
import security.authentication.validator.LoginFormArgumentValidator;
import security.password.PasswordEncoder;


@Service
public class AuthenticationManager implements org.springframework.security.authentication.AuthenticationManager {

    private UserAuthority userAuthority;
    @Autowired
    private JwtBuilder jwtBuilder;

    public boolean authenticate (String email, String password) throws WrongCredential {
        checkCredentials(email, password);
        userAuthority = ProxyUserAuthorityRepository.getRepository().getUserAuthority(email);
        if (userAuthority == null)
            throw new WrongCredential();
        if (!userAuthority.getPassword().equals(password))
            throw new WrongCredential();
        return true;
    }

    public String getJWT () {
        return jwtBuilder.createToken(userAuthority.getUserId());
    }

    public boolean checkCredentials (String email, String password) throws IllegalLoginArgument {
        LoginArgumentValidator validator = new LoginFormArgumentValidator();
        UserAccount userAccount = new UserAccount("", "", email, password);
        return validator.validate(userAccount);
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        return null;
    }
}
