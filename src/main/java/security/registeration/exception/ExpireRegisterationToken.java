package security.registeration.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.REQUEST_TIMEOUT, reason = "token of email confirmation be expired 24 hours after register")
public class ExpireRegisterationToken extends Exception {
}
