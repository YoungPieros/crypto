package security.registeration.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "invalid registeration parameters")
public class RegistrationArgument extends Exception {

    public RegistrationArgument(String message) {
        super(message);
    }

    public RegistrationArgument() {
        super("invalid registeration parameters");
    }

}
