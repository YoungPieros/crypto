package security.registeration.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.CONFLICT, reason = "email address already exist")
public class RepetitiveEmail extends RegistrationArgument {

    public RepetitiveEmail(String message) {
        super(message);
    }

    public RepetitiveEmail() {
        super("email address already exist");
    }

}
