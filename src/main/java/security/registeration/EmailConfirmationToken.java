package security.registeration;


import domain.entity.UnregisterUser;

import java.util.Date;


public class EmailConfirmationToken {

    private String confirmationToken;
    private Date generateDate;
    private UnregisterUser unregisterUser;

    public EmailConfirmationToken (UnregisterUser unregisterUser) {
        this.unregisterUser = unregisterUser;
        this.generateDate = new Date();
        this.confirmationToken = EmailConfirmationTokenGenerator.generateToken();
    }

    public String getConfirmationToken() {
        return confirmationToken;
    }
}
