package security.registeration.validator;

import dto.UserAccount;
import repository.proxy.ProxyAccountRepository;
import repository.proxy.ProxyUnregisterAccountRepository;
import security.registeration.exception.RegistrationArgument;
import security.registeration.exception.RepetitiveEmail;

public class RepetitiveEmailValidator extends RegistrationArgumentValidator {

    private RegistrationArgumentValidator decoratorValidator;

    public RepetitiveEmailValidator () {

    }

    public RepetitiveEmailValidator (RegistrationArgumentValidator decoratorValidator) {
        this.decoratorValidator = decoratorValidator;
    }

    @Override
    public boolean validate(UserAccount userAccount) throws RegistrationArgument {
        if (ProxyAccountRepository.getRepository().existsEmail(userAccount.getEmail()))
            throw new RepetitiveEmail();
        if (ProxyUnregisterAccountRepository.getRepository().existsEmail(userAccount.getEmail()))
            throw new RepetitiveEmail("email address is registered, you should confirm email address");
        if (decoratorValidator != null)
            return decoratorValidator.validate(userAccount);
        return true;
    }
}
