package security.registeration.validator;

import security.registeration.exception.RegistrationArgument;
import dto.UserAccount;

public abstract class RegistrationArgumentValidator extends ArgumentValidator {

    public abstract boolean validate (UserAccount userAccount) throws RegistrationArgument;

}
