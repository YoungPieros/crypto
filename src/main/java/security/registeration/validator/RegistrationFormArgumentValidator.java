package security.registeration.validator;

import dto.UserAccount;
import security.registeration.exception.IllegalRegisterationFormValues;
import security.registeration.exception.RegistrationArgument;

public class RegistrationFormArgumentValidator extends RegistrationArgumentValidator {

    private static final int MINIMUM_PASSWORD_LENGTH = 8;
    private RegistrationArgumentValidator decoratorValidator;

    public RegistrationFormArgumentValidator () {

    }

    public RegistrationFormArgumentValidator (RegistrationArgumentValidator decoratorValidator) {
        this.decoratorValidator = decoratorValidator;
    }

    @Override
    public boolean validate(UserAccount userAccount) throws RegistrationArgument {
        if (this.isArgumentEmpty(userAccount.getFirstName()))
            throw new IllegalRegisterationFormValues();
        if (this.isArgumentEmpty(userAccount.getFirstName()))
            throw new IllegalRegisterationFormValues();
        if (!this.hasMinimumLength(userAccount.getPassword(), MINIMUM_PASSWORD_LENGTH))
            throw new IllegalRegisterationFormValues();
        if (!this.matchesWithPattern(userAccount.getEmail(), EMAIL_REGEX))
            throw new IllegalRegisterationFormValues();
        if (decoratorValidator != null)
            return decoratorValidator.validate(userAccount);
        return true;
    }

}
