package security.registeration.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ArgumentValidator {

    public final String EMAIL_REGEX = "^[\\w-_.+]*[\\w-_.]@([\\w]+\\.)+[\\w]+[\\w]$";

    public boolean isArgumentEmpty (String argument) {
        return argument == null || argument.length() == 0 || argument.trim().length() == 0;
    }

    public boolean hasMinimumLength (String argument, int minimumLength) {
        return argument != null && argument.length() >= minimumLength;
    }

    public boolean matchesWithPattern (String argument, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(argument);
        return matcher.matches();
    }

}
