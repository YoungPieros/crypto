package security.key_factory;

import domain.entity.SecurityKey;

import java.util.UUID;

public class SecurityKeyCreator extends SecurityKeyAbstractFactory {

    private static SecurityKeyCreator creator;

    private SecurityKeyCreator () {

    }

    public static SecurityKeyCreator getInstance() {
        if (creator == null) {
            synchronized (SecurityKeyCreator.class) {
                if (creator == null)
                    creator = new SecurityKeyCreator();
            }
        }
        return creator;
    }

    @Override
    public SecurityKey createKey() {
        SecurityKey securityKey = new SecurityKey();
        securityKey.setSecurityKey(UUID.randomUUID().toString());
        return securityKey;
    }

}
