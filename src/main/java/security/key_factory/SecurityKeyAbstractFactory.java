package security.key_factory;

import domain.entity.SecurityKey;

public abstract class SecurityKeyAbstractFactory {

    public static final int KEY_LENGTH = 1024;

    public abstract SecurityKey createKey ();

}
