package utils;

import java.lang.reflect.Field;

public class Utils {

    public static Object getField (Class classObject, Object object, String privateFieldName) {
        try {
            Field field = classObject.getDeclaredField(privateFieldName);
            field.setAccessible(true);
            return field.get(object);
        }
        catch (NoSuchFieldException | IllegalAccessException e) {
            return null;
        }
    }


}
