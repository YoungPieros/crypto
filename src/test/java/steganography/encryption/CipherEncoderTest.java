package steganography.encryption;

import steganography.encryption.cryption.cryptor.CipherEncoder;
import steganography.exception.SteganographyException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CipherEncoderTest extends CipherEncoder {

    @Test
    public void testEncodingDecoding () throws SteganographyException {
        String secret = "Iman";
        String text = "I am Iman Moradi";
        String encodedText = encrypt(text, secret);
        String decodedText = decrypt(encodedText, secret);
        assertEquals(text, decodedText);
    }

    @Test
    public void testSameHashing () throws SteganographyException {
        String secret = "Iman";
        String text = "I am Iman Moradi";
        String firstEncodedText = encrypt(text, secret);
        String secondEncodedText = encrypt(text, secret);
        assertEquals(firstEncodedText, secondEncodedText);
    }

}
