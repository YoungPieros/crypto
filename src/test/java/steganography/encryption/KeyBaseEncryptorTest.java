package steganography.encryption;

import steganography.encryption.cryption.KeyBaseEncryptor;
import steganography.exception.SteganographyException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class KeyBaseEncryptorTest extends KeyBaseEncryptor {

    @Test
    public void testEncodeDecode() throws SteganographyException {
        String text = "this class extends Kay Base Encryptor that is a facade class to generating decorator classed for ciphering texts with secret key";
        String secret = "imanM";
        String encodedText = encrypt(text, secret);
        String decodedText = decrypt(encodedText, secret);
        assertEquals(text, decodedText);
    }



}
