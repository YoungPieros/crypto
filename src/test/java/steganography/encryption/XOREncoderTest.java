package steganography.encryption;

import steganography.encryption.cryption.cryptor.XOREncoder;
import steganography.exception.SteganographyException;
import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;


public class XOREncoderTest extends XOREncoder {

    @Test
    public void testCreateProperKey () {
        String text = "abc123456789";
        String secret = "imanM";
        String result = "imanMimanMim";
        try {
            Class superClass = getClass().getSuperclass();
            Constructor constructor = superClass.getConstructor();
            Object object = constructor.newInstance();
            Method createProperKeyFunction = null;
            for (Method method: superClass.getDeclaredMethods())
                if (method.getName().equals("createProperKey")) {
                    createProperKeyFunction = method;
                    break;
                }
            createProperKeyFunction.setAccessible(true);
            assertEquals(result, createProperKeyFunction.invoke(object, text, secret));
        }
        catch (NoSuchMethodException e) {
            fail("method createProperKey not found");
        }
        catch (IllegalAccessException e) {
            fail("illegal createProperKey method argumants");
        }
        catch (InstantiationException e) {
            fail("can not instantiate XorEncoder");
        }
        catch (InvocationTargetException e) {
            fail("can not create new instance");
        }
    }

    @Test
    public void testEncodeDecode() throws SteganographyException {
        String text = "abc123456789";
        String secret = "imanM";
        String encodedText = encrypt(text, secret);
        String decodedText = decrypt(encodedText, secret);
        assertEquals(text, decodedText);
    }

}
