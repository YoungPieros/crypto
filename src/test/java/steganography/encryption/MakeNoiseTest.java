package steganography.encryption;

import org.junit.Test;
import steganography.encryption.steganography.encryptor.SteganographyEncoder;
import steganography.encryption.steganography.encryptor.Utils;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Random;

import static org.junit.Assert.fail;

public class MakeNoiseTest {

    @Test
    public void test () {
        int TEST_CASES = 50;
        Random randomGenerator = new Random();
        for (int i = 0; i < TEST_CASES; i++) {
            int redValue = randomGenerator.nextInt(256);
            int greenValue = randomGenerator.nextInt(256);
            int blueValue = randomGenerator.nextInt(256);
            Color mainColor = new Color(redValue, greenValue, blueValue);
            Color noiseColor = Utils.createNoiseColor(mainColor);
            if (Math.abs(redValue - noiseColor.getRed()) > 1 ||
                Math.abs(greenValue - noiseColor.getGreen()) > 1 ||
                Math.abs(blueValue - noiseColor.getBlue()) > 1)
                fail("main Color: " + mainColor + "\nnoise color: " + noiseColor);
        }
    }

}
