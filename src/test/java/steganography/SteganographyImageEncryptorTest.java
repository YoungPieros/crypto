package steganography;

import steganography.encryption.SteganographyImageEncryptor;
import steganography.encryption.steganography.encryptor.SteganographyEncoder;
import steganography.encryption.validator.SteganographyImageSizeValidator;
import steganography.encryption.validator.SteganographyPasswordValidator;
import steganography.encryption.validator.SteganographyTextValidator;
import steganography.exception.NotSteganographyImage;
import steganography.exception.SteganographyException;
import domain.entity.SecurityKey;
import dto.SteganographyImageDto;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.core.io.ClassPathResource;
import utils.Utils;

import static org.junit.Assert.assertEquals;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;


public class SteganographyImageEncryptorTest extends SteganographyImageEncryptor {


    private static final String SECRET_KEY = "Iman";
    private static final String SECRET_TEXT = "this is unit test for steganography unit";
    private static final String PASSWORD = "RandomPassword123456798";

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    public SteganographyImageEncryptorTest () {
        super(null, null);
    }


    public static SecurityKey createSecretKey (String secretKey) {
        SecurityKey securityKey = new SecurityKey();
        securityKey.setSecurityKey(secretKey);
        return securityKey;
    }

    public static SteganographyImageDto createSteganographyDto (String text, String password, String fileName) throws IOException {
        File image = new ClassPathResource(fileName).getFile();
        BufferedImage bufferedImage = ImageIO.read(image);
        String contentType = extractContentType(fileName);
        return new SteganographyImageDto(fileName, password, text, contentType, bufferedImage);
    }

    private static String extractContentType (String fileName) {
        return "image/" + ((fileName.split("\\."))[1]);
    }

    @Test
    public void testIllegalImageSize () throws IOException, SteganographyException {
        expectedException.expect(SteganographyException.class);
        SteganographyImageSizeValidator validator = new SteganographyImageSizeValidator(null);
        Integer minWidth = (Integer) Utils.getField(SteganographyImageSizeValidator.class, validator, "MIN_WIDTH");
        Integer minHeight = (Integer) Utils.getField(SteganographyImageSizeValidator.class, validator, "MIN_HEIGHT");
        expectedException.expectMessage("image size must be at least " + minWidth + "(width) * " + minHeight + "(height)");
        SteganographyImageDto imageDto = createSteganographyDto("text", "12345678", "google-logo.png");
        SecurityKey securityKey = createSecretKey(SECRET_KEY);
        SteganographyImageEncryptor encryptor = new SteganographyImageEncryptor(securityKey, imageDto);
        encryptor.encrypt();
    }

    @Test
    public void testEmptyPassword () throws IOException, SteganographyException {
        expectedException.expect(SteganographyException.class);
        SteganographyPasswordValidator validator = new SteganographyPasswordValidator(null);
        Integer minPasswordLength = (Integer) Utils.getField(SteganographyPasswordValidator.class, validator, "MIN_PASSWORD_LENGTH");
        expectedException.expectMessage("password length must be greater than " + minPasswordLength + " character");
        SteganographyImageDto imageDto = createSteganographyDto("text", "", "S-uncle.png");
        SecurityKey securityKey = createSecretKey(SECRET_KEY);
        SteganographyImageEncryptor encryptor = new SteganographyImageEncryptor(securityKey, imageDto);
        encryptor.encrypt();
    }

    @Test
    public void testNullPassword () throws IOException, SteganographyException {
        expectedException.expect(SteganographyException.class);
        expectedException.expectMessage("password is empty");
        SteganographyImageDto imageDto = createSteganographyDto("text", null, "S-uncle.png");
        SecurityKey securityKey = createSecretKey(SECRET_KEY);
        SteganographyImageEncryptor encryptor = new SteganographyImageEncryptor(securityKey, imageDto);
        encryptor.encrypt();
    }

    @Test
    public void testIllegalMinimumPasswordLength () throws IOException, SteganographyException {
        expectedException.expect(SteganographyException.class);
        SteganographyPasswordValidator validator = new SteganographyPasswordValidator(null);
        Integer minPasswordLength = (Integer) Utils.getField(SteganographyPasswordValidator.class, validator, "MIN_PASSWORD_LENGTH");
        expectedException.expectMessage("password length must be greater than " + minPasswordLength + " character");
        SteganographyImageDto imageDto = createSteganographyDto("text", "1234567", "S-uncle.png");
        SecurityKey securityKey = createSecretKey(SECRET_KEY);
        SteganographyImageEncryptor encryptor = new SteganographyImageEncryptor(securityKey, imageDto);
        encryptor.encrypt();
    }

    @Test
    public void testEmptyText () throws IOException, SteganographyException {
        expectedException.expect(SteganographyException.class);
        expectedException.expectMessage("secret text is empty");
        SteganographyImageDto imageDto = createSteganographyDto("", "12345678", "S-uncle.png");
        SecurityKey securityKey = createSecretKey(SECRET_KEY);
        SteganographyImageEncryptor encryptor = new SteganographyImageEncryptor(securityKey, imageDto);
        encryptor.encrypt();
    }


    @Test
    public void testExtraTextSize () throws IOException, SteganographyException {
        expectedException.expect(SteganographyException.class);
        SteganographyTextValidator validator = new SteganographyTextValidator(null);
        Integer textLimitSize = (Integer) Utils.getField(SteganographyTextValidator.class, validator, "TEXT_LIMIT_SIZE");
        expectedException.expectMessage("secret text must be less than " + textLimitSize + " character");
        String text = "a".repeat(textLimitSize + 1);
        SteganographyImageDto imageDto = createSteganographyDto(text, "12345678", "S-uncle.png");
        SecurityKey securityKey = createSecretKey(SECRET_KEY);
        SteganographyImageEncryptor encryptor = new SteganographyImageEncryptor(securityKey, imageDto);
        encryptor.encrypt();
    }

    @Test
    public void integrateTest () throws IOException, SteganographyException {
        String fileName = "S-uncle.png";
        SteganographyImageDto pureImageDto = createSteganographyDto(SECRET_TEXT, PASSWORD, fileName);
        SecurityKey securityKey = createSecretKey(SECRET_KEY);
        SteganographyImageEncryptor encryptor = new SteganographyImageEncryptor(securityKey, pureImageDto);
        encryptor.encrypt();
        SteganographyImageDto encodedImageDto = new SteganographyImageDto(fileName, PASSWORD, "", extractContentType(fileName), pureImageDto.getBufferedImage());
        SteganographyImageEncryptor decryptor = new SteganographyImageEncryptor(securityKey, encodedImageDto);
        decryptor.decrypt();
        assertEquals(SECRET_TEXT, decryptor.getDecodedText());
    }

    @Test
    public void notSteganographyFile () throws IOException, SteganographyException {
        expectedException.expect(NotSteganographyImage.class);
        expectedException.expectMessage("bad steganography file");
        String fileName = "S-uncle.png";
        SteganographyImageDto pureImageDto = createSteganographyDto(SECRET_TEXT, PASSWORD, fileName);
        SecurityKey securityKey = createSecretKey(SECRET_KEY);
        SteganographyImageEncryptor encryptor = new SteganographyImageEncryptor(securityKey, pureImageDto);
        encryptor.decrypt();
    }


}
