package steganography;

import steganography.encryption.steganography.secret_number.SecretNumberGenerator;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.ArrayList;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class SecretNumberImplTest {

    private final int pictureResolution = 100 * 200;
    private final String secret = "Iman";
    private int secretNumber;

    @Before
    public void generatePrimesTest () {
        secretNumber = SecretNumberGenerator.getInstance().generateNumber(secret, pictureResolution);
    }

    @Test
    public void checkSecretKeyBePrime () {
        for (int i = 2; i < secretNumber; i++)
            if (secretNumber < i * i)
                return;
            else if (secretNumber % i == 0)
                fail("secret key is not prime");
    }

    @Test public void checkSecretKeyBiggerThanResolution () {
        assertTrue(secretNumber > pictureResolution);
    }

    @Test public void checkSecretKeyLessThanResolutionCoefficient () {
        assertTrue(secretNumber <= 3 * pictureResolution);
    }

    @Test
    public void checkPrimeExtendRangeNumbers () throws NoSuchFieldException, IllegalAccessException {
        SecretNumberGenerator generator = SecretNumberGenerator.getInstance();
        Field primesField = SecretNumberGenerator.class.getDeclaredField("primes");
        primesField.setAccessible(true);
        ArrayList<Integer> primes = (ArrayList<Integer>) primesField.get(generator);
        assertTrue(3.9 * pictureResolution < primes.get(primes.size()-1));
    }


}
